import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CalendarService {

	constructor(private afs: AngularFirestore) { }

	getCalendar() {
		return this.afs.collection('Calendar');
	}

	getCalendarById(id: string) {
		return this.afs.doc('Calendar/' + id);
	}

	getEvents(calendarDocId) {
		return this.getCalendarById(calendarDocId).collection('Events');
	}

}
