import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/combineLatest';

import { CalendarService} from '../calendar.service';

interface Calendar {
	date: string;
	calendarId: string;
	events: Array<Event>;
}

interface Event {
	title: string;
	time: string;
	eventId: string;
}

@Component({
	selector: 'app-calendar',
	templateUrl: './calendar.component.html',
	styleUrls: ['./calendar.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit {
	calendarCol: AngularFirestoreCollection<Calendar>;
	eventsCol: AngularFirestoreCollection<Event>;
	calendar: any;
	event: any;
	calendarDoc: AngularFirestoreDocument<Calendar>;

	id: string;

	constructor(private afs: AngularFirestore, private calendarService: CalendarService) { }

	ngOnInit() {

		this.calendar = this.calendarService.getCalendar().snapshotChanges()
		.map(calendarSnaps => {
			return calendarSnaps.map(calendar => {
				const calendarData = calendar.payload.doc.data() as Calendar;
				const calendarId = calendar.payload.doc.id;
				return this.calendarService.getEvents(calendarId)
				.snapshotChanges()
				.map(eventSnaps => {
					return eventSnaps.map(event => {
						const eventData = event.payload.doc.data() as Event;
						const eventId = event.payload.doc.id;
						return { eventId, eventData };
					});
				})
				.map(events => {
					return { calendarId, calendarData, events: events}
				});
			})
		})
		.flatMap(calendars => {return Observable.combineLatest(calendars)});
	}
}