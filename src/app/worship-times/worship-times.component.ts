import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-worship-times',
  templateUrl: './worship-times.component.html',
  styleUrls: ['./worship-times.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class WorshipTimesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
