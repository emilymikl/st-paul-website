import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { WorshipTimesComponent } from './worship-times/worship-times.component';
import { CalendarComponent} from './calendar/calendar.component';
import { CalendarService } from './calendar.service';

var firebaseConfig = {    
  apiKey: "AIzaSyDQoAF1HnOQ1rpF-Ub3jjeU28J7iXSoOWM",
  authDomain: "st-paul-website.firebaseapp.com",
  databaseURL: "https://st-paul-website.firebaseio.com",
  projectId: "st-paul-website",
  storageBucket: "st-paul-website.appspot.com",
  messagingSenderId: "297344625835"
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WorshipTimesComponent,
    CalendarComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    FormsModule
  ],
  providers: [
    CalendarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
